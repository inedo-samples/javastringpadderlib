# Java StringPadderLib Maven Sample
This is a sample java application using Maven to create a MAven Library.

## What you’ll need
+ A favorite text editor or IDE (this was build using Visual Studio Code and the [Extension Pack for Java](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-pack))
+ [OpenJDK 1.17](https://openjdk.org/projects/jdk/17/)
+ Maven
+ ProGet (to deploy your Maven package and import your SBOM)

### Install Maven.
+ [Install Maven on Windows](https://www.baeldung.com/install-maven-on-windows-linux-mac#installing-maven-on-windows)
+ [Install Maven on Linux](https://www.baeldung.com/install-maven-on-windows-linux-mac#installing-maven-on-linux)
+ [Install Maven on Mac OSX](https://www.baeldung.com/install-maven-on-windows-linux-mac#installing-maven-on-mac-os-x)

## Settings.xml Setup
You Maven settings (`.m2\settings.xml`) will need to include the following:
1. Create an active profile called ProGet
2. Add your `repositories` and `pluginRepositories`.  Example:
   ```xml
   <repositories>
       <repository>
           <id>central</id>
           <url>http://localhost:81/maven2/public-maven</url>
           <snapshots>
               <enabled>false</enabled>
           </snapshots>
       </repository>
       <repository>
           <id>snapshots</id>
           <url>http://localhost:81/maven2/public-maven</url>
           <releases>
               <enabled>false</enabled>
           </releases>
       </repository>
   </repositories>
   <pluginRepositories>
       <pluginRepository>
           <id>central</id>
           <url>http://localhost:81/maven2/public-maven</url>
           <snapshots>
               <enabled>false</enabled>
           </snapshots>
       </pluginRepository>
       <pluginRepository>
           <id>snapshots</id>
           <url>http://localhost:81/maven2/public-maven</url>
           <releases>
               <enabled>false</enabled>
           </releases>
       </pluginRepository>
   </pluginRepositories>
   ```
3. Your servers node should contain your api key (this is also used by the `distributionManagement` in your pom file). Example:
   ```xml
   <servers>
       <server>
           <id>central</id>
           <username>api</username>
           <password>my-api-key</password>
       </server>
       <server>
           <id>snapshots</id>
           <username>api</username>
           <password>my-api-key</password>
       </server>
   </servers>
   ```
4. To prevent the need to make code changes to compile and deploy this application to ProGet, we need to configure a property in your `settings.xml` to tell Maven how to communicate with ProGet.  The required properties are:
    1. ProGet Maven Repository: `${distribution-repository}`
   Example:
   ```xml
   <properties>
       <distribution-repository>http://localhost:81/maven2/public-maven/</distribution-repository>
   </properties>
   ```
Once you have configured all of this, you can now run the following commands to install, compile, and deploy your jar to ProGet:
1. `mvn install`
1. `mvn compile`
1. `mvn deploy`
